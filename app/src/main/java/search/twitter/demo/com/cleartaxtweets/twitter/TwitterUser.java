package search.twitter.demo.com.cleartaxtweets.twitter;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class TwitterUser implements Parcelable{

	@SerializedName("screen_name")
	private String screenName;
	
	@SerializedName("name")
	private String name;
	
	@SerializedName("profile_image_url")
	private String profileImageUrl;

	protected TwitterUser(Parcel in) {
		screenName = in.readString();
		name = in.readString();
		profileImageUrl = in.readString();
	}

	public static final Creator<TwitterUser> CREATOR = new Creator<TwitterUser>() {
		@Override
		public TwitterUser createFromParcel(Parcel in) {
			return new TwitterUser(in);
		}

		@Override
		public TwitterUser[] newArray(int size) {
			return new TwitterUser[size];
		}
	};

	public String getProfileImageUrl() {
		return profileImageUrl;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setProfileImageUrl(String profileImageUrl) {
		this.profileImageUrl = profileImageUrl;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(screenName);
		dest.writeString(name);
		dest.writeString(profileImageUrl);
	}
}
