package search.twitter.demo.com.cleartaxtweets;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.misc.AsyncTask;
import com.android.volley.request.StringRequest;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import search.twitter.demo.com.cleartaxtweets.twitter.Authenticated;
import search.twitter.demo.com.cleartaxtweets.twitter.SearchResults;
import search.twitter.demo.com.cleartaxtweets.twitter.Searches;
import search.twitter.demo.com.cleartaxtweets.twitter.Utils;
import search.twitter.demo.com.cleartaxtweets.twitter.WordCount;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private SharedPreferences mSharedPreferences;
    private View mProgressView;
    private Searches searches;
    private Tweetadapter tweetadapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mProgressView = findViewById(R.id.progress);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        tweetadapter = new Tweetadapter(this);
        recyclerView.setAdapter(tweetadapter);
        showProgress(this,true,recyclerView,mProgressView);

        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo == null || !networkInfo.isConnected()) {
            Log.v(TAG, "No network connection available.");
            return;
        }
        if(!CheckAuth()){
            authreq();
        }else{
            getTweeks();
        }
    }


    private boolean CheckAuth(){
        return !TextUtils.isEmpty(mSharedPreferences.getString(Utils.PREF_ACCESS_TOKEN, "")) && !TextUtils.isEmpty(mSharedPreferences.getString(Utils.PREF_TOKEN_TYPE, ""));
    }
    private void authreq(){
        final String mRequestBody = "grant_type=client_credentials";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Utils.TOKENURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG,"response=="+response);
                Gson gson = new Gson();
                Authenticated auth = gson.fromJson(response, Authenticated.class);
                SharedPreferences.Editor editor =mSharedPreferences.edit();
                editor.putString(Utils.PREF_TOKEN_TYPE,auth.token_type);
                editor.putString(Utils.PREF_ACCESS_TOKEN,auth.access_token);
                editor.commit();
                getTweeks();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                String urlApiKey = null;
                String urlApiSecret = null;
                try {
                    urlApiKey = URLEncoder.encode(Utils.KEY, "UTF-8");
                    urlApiSecret = URLEncoder.encode(Utils.SECRET, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                // Concatenate the encoded consumer key, a colon character, and the encoded consumer secret
                String combined = urlApiKey + ":" + urlApiSecret;

                // Base64 encode the string
                String base64Encoded = Base64.encodeToString(combined.getBytes(), Base64.NO_WRAP);
                Map<String,String> headers = new HashMap<>();
                headers.put("Authorization", "Basic " + base64Encoded);
                headers.put("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return mRequestBody == null ? null : mRequestBody.getBytes(Utils.PROTOCOL_CHARSET);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return  null;
            }
        };
        stringRequest.setShouldCache(false);
        TweetApplication.getInstance().getRequestQue().add(stringRequest);
    }


    private void getTweeks() {
        String token_type = mSharedPreferences.getString(Utils.PREF_TOKEN_TYPE,"");
        final String access_token = mSharedPreferences.getString(Utils.PREF_ACCESS_TOKEN,"");
        if ( token_type.equals("bearer")) {
            StringRequest stringRequest = new StringRequest(Request.Method.GET, Utils.SEARCHURL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i(TAG,"response="+response);
                    Gson gson = new Gson();
                    SearchResults sr = gson.fromJson(response, SearchResults.class);
                    searches = sr.getStatuses();
                    showProgress(MainActivity.this,false,recyclerView,mProgressView);
                    tweetadapter.setData(searches);
                    tweetadapter.notifyDataSetChanged();
                    new WordAsync().execute(searches);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    showProgress(MainActivity.this,false,recyclerView,mProgressView);
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> map = new HashMap<>();
                    map.put("q" ,Utils.SearchString);
                    map.put("result_type" ,"recent");
                    map.put("count" ,"100");
                    return map;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> map = new HashMap<>();
                    map.put("Authorization", "Bearer " +access_token);
                    map.put("Content-Type", "application/json");
                    return map;
                }
            };
            stringRequest.setShouldCache(false);
            TweetApplication.getInstance().getRequestQue().add(stringRequest);
        }

    }

    public static void showProgress(Context mContext, final boolean show, final View view, final View progressBar) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = mContext.getResources().getInteger(android.R.integer.config_shortAnimTime);

            view.setVisibility(show ? View.GONE : View.VISIBLE);
            view.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    view.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            progressBar.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            view.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    private static class Tweetadapter extends RecyclerView.Adapter<Tweetadapter.TweetHolder>{

        Context mContext;
        Searches searches;
        Tweetadapter(Context mContext){
            this.mContext = mContext;
        }
        @Override
        public TweetHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_tweet,parent,false);
            return new TweetHolder(view);
        }

        @Override
        public void onBindViewHolder(TweetHolder holder, int position) {
                holder.textView_tweet.setText(searches.get(position).toString());
        }

        public  void setData(Searches searches){
            this.searches = searches;
        }
        @Override
        public int getItemCount() {
            return searches!=null ? searches.size():0;
        }

         static class TweetHolder extends RecyclerView.ViewHolder{
             protected TextView textView_tweet;
             protected TweetHolder(View itemView) {
                super(itemView);
                textView_tweet = (TextView) itemView;
            }
        }
    }

    private class WordAsync extends AsyncTask<Searches , Void, List<Map.Entry<String,WordCount.Frequency>>>{

        @Override
        protected List<Map.Entry<String,WordCount.Frequency>> doInBackground(Searches... params) {
            WordCount wordCount = new WordCount();
            Map<String,WordCount.Frequency> wordMap=wordCount.getWordCount((Searches) params[0]);
            Log.i(TAG,"=="+wordMap.size());
            //sort map
            Set<Map.Entry<String, WordCount.Frequency>> set = wordMap.entrySet();
            List<Map.Entry<String, WordCount.Frequency>> list = new ArrayList<Map.Entry<String, WordCount.Frequency>>(set);
            Collections.sort( list, new Comparator<Map.Entry<String, WordCount.Frequency>>() {
                public int compare(Map.Entry<String, WordCount.Frequency> o1, Map.Entry<String, WordCount.Frequency> o2 )
                {
                    int lhs = o1.getValue().getFrequency();
                    int rhs = o2.getValue().getFrequency();
                    return ( lhs == rhs ? 0 : lhs > rhs ? -1:1);
                }
            } );
            return list;
        }

        @Override
        protected void onPostExecute(List<Map.Entry<String,WordCount.Frequency>> entryList) {
            super.onPostExecute(entryList);
            String topmost = entryList.get(0).getKey()+"\n"+
                    entryList.get(1).getKey()+"\n"+
                    entryList.get(2).getKey()+"\n";
            tweetadapter.notifyItemChanged(0);
            showDialog(topmost);

        }
    }
    private void showDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(getString(R.string.dialog_title));
        builder.setMessage(message);
        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();
    }
}
