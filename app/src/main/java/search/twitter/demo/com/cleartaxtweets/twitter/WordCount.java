package search.twitter.demo.com.cleartaxtweets.twitter;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Created by nkhunt on 8/4/2016.
 */
public class WordCount {

    public Map<String,Frequency> getWordCount(Searches searches){

        StringBuilder stringBuilder = new StringBuilder();
        Map<String,Frequency> wordMap = new HashMap<>();
        for (Search search : searches) {
            stringBuilder.append(search.toString());
        }

        String replaceChars = stringBuilder.toString().replaceAll("[-+.^:,]","");
        StringTokenizer st = new StringTokenizer(replaceChars, " ");
        Log.i("ddd",replaceChars.substring(0,replaceChars.length()/2));
        Log.i("ddd33",replaceChars.substring(replaceChars.length()/2 + 1));
        while (st.hasMoreTokens()) {
            String tmp = st.nextToken().toLowerCase();
            Frequency token = wordMap.get(tmp);
            if (token == null) {
                if(!tmp.contains("…"))
                    wordMap.put(tmp, new Frequency());
            } else {
                token.incrementFrequency();
            }
        }
        return wordMap;
    }



    public class Frequency{

        private Frequency(){
            this.count = 1;
        }

        private int count;

        public int getFrequency(){
            return this.count;
        }

        public void incrementFrequency(){
            this.count++;
        }

        public void setCount(int count){
            this.count = count;
        }
    }
}
