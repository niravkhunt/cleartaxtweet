package search.twitter.demo.com.cleartaxtweets.twitter;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

// twitter search results
public class SearchResults implements Parcelable{

	@SerializedName("statuses")
	private Searches statuses;

	@SerializedName("search_metadata")
	private SearchMetadata metadata;


	protected SearchResults(Parcel in) {
		metadata = in.readParcelable(SearchMetadata.class.getClassLoader());
	}

	public static final Creator<SearchResults> CREATOR = new Creator<SearchResults>() {
		@Override
		public SearchResults createFromParcel(Parcel in) {
			return new SearchResults(in);
		}

		@Override
		public SearchResults[] newArray(int size) {
			return new SearchResults[size];
		}
	};

	public Searches getStatuses() {
		return statuses;
	}

	public void setStatuses(Searches statuses) {
		this.statuses = statuses;
	}

	public SearchMetadata getMetadata() {
		return metadata;
	}

	public void setMetadata(SearchMetadata metadata) {
		this.metadata = metadata;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeParcelable(metadata, flags);
	}
}
