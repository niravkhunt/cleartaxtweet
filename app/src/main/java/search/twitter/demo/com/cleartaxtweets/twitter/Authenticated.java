package search.twitter.demo.com.cleartaxtweets.twitter;

import android.os.Parcel;
import android.os.Parcelable;

public class Authenticated implements Parcelable{
	public String token_type;
	public String access_token;

	protected Authenticated(Parcel in) {
		token_type = in.readString();
		access_token = in.readString();
	}

	public static final Creator<Authenticated> CREATOR = new Creator<Authenticated>() {
		@Override
		public Authenticated createFromParcel(Parcel in) {
			return new Authenticated(in);
		}

		@Override
		public Authenticated[] newArray(int size) {
			return new Authenticated[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(token_type);
		dest.writeString(access_token);
	}
}
