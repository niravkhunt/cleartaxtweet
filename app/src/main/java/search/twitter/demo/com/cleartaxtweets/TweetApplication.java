package search.twitter.demo.com.cleartaxtweets;

import android.app.Application;

import com.android.volley.RequestQueue;
import com.android.volley.RequestTickle;
import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.Volley;
import com.android.volley.toolbox.VolleyTickle;

/**
 * Created by nkhunt on 8/4/2016.
 */
public class TweetApplication extends Application {
    RequestQueue mRequestQueue;
    RequestTickle mRequestTickle;
    public static TweetApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public  RequestQueue getRequestQue(){
        if(mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public RequestTickle getRequestTickle() {
        if (mRequestTickle == null) {
            mRequestTickle = VolleyTickle.newRequestTickle(getApplicationContext());
        }

        return mRequestTickle;
    }

    public static synchronized TweetApplication getInstance() {
        return instance;
    }
}
