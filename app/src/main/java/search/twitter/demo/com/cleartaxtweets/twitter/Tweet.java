package search.twitter.demo.com.cleartaxtweets.twitter;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Tweet implements Parcelable{

	@SerializedName("created_at")
	private String DateCreated;

	@SerializedName("id")
	private String Id;

	@SerializedName("text")
	private String Text;

	@SerializedName("in_reply_to_status_id")
	private String InReplyToStatusId;

	@SerializedName("in_reply_to_user_id")
	private String InReplyToUserId;

	@SerializedName("in_reply_to_screen_name")
	private String InReplyToScreenName;

	@SerializedName("user")
	private TwitterUser User;

	protected Tweet(Parcel in) {
		DateCreated = in.readString();
		Id = in.readString();
		Text = in.readString();
		InReplyToStatusId = in.readString();
		InReplyToUserId = in.readString();
		InReplyToScreenName = in.readString();
	}

	public static final Creator<Tweet> CREATOR = new Creator<Tweet>() {
		@Override
		public Tweet createFromParcel(Parcel in) {
			return new Tweet(in);
		}

		@Override
		public Tweet[] newArray(int size) {
			return new Tweet[size];
		}
	};

	public String getDateCreated() {
		return DateCreated;
	}
	
	public String getId() {
		return Id;
	}

	public String getInReplyToScreenName() {
		return InReplyToScreenName;
	}

	public String getInReplyToStatusId() {
		return InReplyToStatusId;
	}

	public String getInReplyToUserId() {
		return InReplyToUserId;
	}

	public String getText() {
		return Text;
	}

	public void setDateCreated(String dateCreated) {
		DateCreated = dateCreated;
	}

	public void setId(String id) {
		Id = id;
	}

	public void setInReplyToScreenName(String inReplyToScreenName) {
		InReplyToScreenName = inReplyToScreenName;
	}
	
	public void setInReplyToStatusId(String inReplyToStatusId) {
		InReplyToStatusId = inReplyToStatusId;
	}
	
	public void setInReplyToUserId(String inReplyToUserId) {
		InReplyToUserId = inReplyToUserId;
	}
	
	public void setText(String text) {
		Text = text;
	}

	public void setUser(TwitterUser user) {
		User = user;
	}

	public TwitterUser getUser() {
		return User;
	}

	@Override
	public String  toString(){
		return getText();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(DateCreated);
		dest.writeString(Id);
		dest.writeString(Text);
		dest.writeString(InReplyToStatusId);
		dest.writeString(InReplyToUserId);
		dest.writeString(InReplyToScreenName);
	}
}
