package search.twitter.demo.com.cleartaxtweets.twitter;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

// search metadata
public class SearchMetadata implements Parcelable{

	@SerializedName("completed_in")
	private Float CompletedIn;

	@SerializedName("max_id")
	private long MaxId;

	@SerializedName("max_id_str")
	private String MaxIdStr;

	@SerializedName("next_results")
	private String NextResults;

	@SerializedName("query")
	private String Query;

	@SerializedName("refresh_url")
	private String RefreshUrl;

	@SerializedName("count")
	private long Count;

	@SerializedName("since_id")
	private long SinceId;

	@SerializedName("since_id_str")
	private String SinceIdStr;

	protected SearchMetadata(Parcel in) {
		MaxId = in.readLong();
		MaxIdStr = in.readString();
		NextResults = in.readString();
		Query = in.readString();
		RefreshUrl = in.readString();
		Count = in.readLong();
		SinceId = in.readLong();
		SinceIdStr = in.readString();
	}

	public static final Creator<SearchMetadata> CREATOR = new Creator<SearchMetadata>() {
		@Override
		public SearchMetadata createFromParcel(Parcel in) {
			return new SearchMetadata(in);
		}

		@Override
		public SearchMetadata[] newArray(int size) {
			return new SearchMetadata[size];
		}
	};

	public Float getCompletedIn() {
		return CompletedIn;
	}

	public void setCompletedIn(Float completedIn) {
		CompletedIn = completedIn;
	}

	public long getMaxId() {
		return MaxId;
	}

	public void setMaxId(long maxId) {
		MaxId = maxId;
	}

	public String getMaxIdStr() {
		return MaxIdStr;
	}

	public void setMaxIdStr(String maxIdStr) {
		MaxIdStr = maxIdStr;
	}

	public String getNextResults() {
		return NextResults;
	}

	public void setNextResults(String nextResults) {
		NextResults = nextResults;
	}

	public String getQuery() {
		return Query;
	}

	public void setQuery(String query) {
		Query = query;
	}

	public String getRefreshUrl() {
		return RefreshUrl;
	}

	public void setRefreshUrl(String refreshUrl) {
		RefreshUrl = refreshUrl;
	}

	public long getCount() {
		return Count;
	}

	public void setCount(long count) {
		Count = count;
	}

	public long getSinceId() {
		return SinceId;
	}

	public void setSinceId(long sinceId) {
		SinceId = sinceId;
	}

	public String getSinceIdStr() {
		return SinceIdStr;
	}

	public void setSinceIdStr(String sinceIdStr) {
		SinceIdStr = sinceIdStr;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(MaxId);
		dest.writeString(MaxIdStr);
		dest.writeString(NextResults);
		dest.writeString(Query);
		dest.writeString(RefreshUrl);
		dest.writeLong(Count);
		dest.writeLong(SinceId);
		dest.writeString(SinceIdStr);
	}
}
